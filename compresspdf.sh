#Script for compressing pdf files with ghostscript
#sdmg@technologyec.com 2019-12-20

path="/var/www/"


# variables
{
    count=0
    total=$(find $path -type f -name "*.pdf" | wc -l)
    log=log
	res=120
}

#open the directory to process each pdf
cd $path

find * -type f -name "*.pdf" | while read -r file
do

  ((count++))
  echo "Procesing pdf #$count of $total " | tee -a $log
  echo "Current PDF  : $file "| tee -a $log
  
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dColorImageResolution=$res -dGrayImageDownsampleType=/Bicubic -dGrayImageResolution=$res -dMonoImageDownsampleType=/Subsample -dMonoImageResolution=$res -dNOPAUSE -dBATCH -dQUIET -sOutputFile="new_$file" "$file" | tee -a $log

  sizeold=$(wc -c "$file" | cut -d' ' -f1)
  sizenew=$(wc -c "new_$file" | cut -d' ' -f1)
  difference=$((sizenew-sizeold))

  if [ $difference -lt 0 ]
      then
          rm "$file"
          mv "new_$file" "$file"
          printf "Success.  Previous pdf size %'.f  New pdf size %'.f  difference %'.f \n" $((sizeold)) $((sizenew)) $((-difference)) | tee -a $log
  else
       rm "new_$file"
       echo "No compression needed" | tee -a $log
  fi
done


